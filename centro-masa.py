import pandas as pd
import numpy as np
import folium

df = pd.read_csv('./historico-pedidos-cadetes.csv')

promedio_negocio_lat = df['negocio_latitud'].mean()
promedio_negocio_lon = df['negocio_longitud'].mean()

c = folium.Map(location=[-38.9805793,-61.3048407], zoom_start=14)

folium.Marker(
        location=[promedio_negocio_lat, promedio_negocio_lon],
        icon=folium.Icon(color='red', icon='info-sign')
).add_to(c)

c.save('centro-masa.html')

print(f'Centro de masa ideal para esperar: ({promedio_negocio_lat}, {promedio_negocio_lon})')
