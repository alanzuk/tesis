import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
from sklearn.metrics import mean_absolute_error, mean_squared_error
import numpy as np


# Cargar los datos limpios
data = pd.read_csv('./combined_data_clean.csv')

# Asumimos que 'distancia', 'hora_del_dia', y 'dia_de_la_semana' ya están en 'data'
X = data[['distancia', 'hora_del_dia', 'dia_de_la_semana','pedidos_en_simultaneo']]
# X = data[['distancia']]

y = data['tiempo_entrega_minutos']

# Dividir los datos en entrenamiento y prueba
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Normalización de características
scaler = StandardScaler()
X_train_scaled = scaler.fit_transform(X_train)
X_test_scaled = scaler.transform(X_test)

# Construir el modelo
model = Sequential([
    Dense(64, activation='relu', input_shape=(X_train_scaled.shape[1],)),
    Dense(64, activation='relu'),
    Dense(1)
])

model.compile(optimizer='adam', loss='mean_squared_error')

# Entrenar el modelo
model.fit(X_train_scaled, y_train, validation_split=0.2, epochs=100, batch_size=32)

# Evaluar el modelo
predicciones_nn = model.predict(X_test_scaled)

# Asegúrate de que las predicciones son un array 1D para que coincidan con la forma de y_test
predicciones_nn = predicciones_nn.flatten()

# Calcular y mostrar MAE
mae_nn = mean_absolute_error(y_test, predicciones_nn)
print(f"MAE (Error Absoluto Medio): {mae_nn}")

# Calcular y mostrar MSE
mse_nn = mean_squared_error(y_test, predicciones_nn)
print(f"MSE (Error Cuadrático Medio): {mse_nn}")

# Calcular y mostrar RMSE
rmse_nn = np.sqrt(mse_nn)
print(f"RMSE (Raíz del Error Cuadrático Medio): {rmse_nn}")

# Opcionalmente, calcular y mostrar R² - Coeficiente de determinación
from sklearn.metrics import r2_score
r2_nn = r2_score(y_test, predicciones_nn)
print(f"R² (Coeficiente de determinación): {r2_nn}")
val_loss = model.evaluate(X_test_scaled, y_test)
print(f'Loss en el conjunto de prueba: {val_loss}')



distancia_nuevo_pedido = 0.81  # Ejemplo de distancia calculada
hora_del_dia_nuevo_pedido = 23  # 2 PM
dia_de_la_semana_nuevo_pedido = 3  # Miércoles
pedidos_en_simultaneo = 2

# # Crear un DataFrame o arreglo con estas características
nuevas_caracteristicas = pd.DataFrame([[distancia_nuevo_pedido, hora_del_dia_nuevo_pedido, dia_de_la_semana_nuevo_pedido, pedidos_en_simultaneo]],
                                       columns=['distancia', 'hora_del_dia', 'dia_de_la_semana', 'pedidos_en_simultaneo'])

nuevas_caracteristicas_scaled = scaler.transform(nuevas_caracteristicas)  # Asumiendo que usaste StandardScaler


predicciones_nuevo_pedido = model.predict(nuevas_caracteristicas_scaled)
    
    # Imprime la predicción para cada modelo
print(f"Predicción de pedido: {predicciones_nuevo_pedido[0]} minutos")