import folium
import pandas as pd

import pandas as pd
from math import radians, cos, sin, sqrt, atan2
import numpy as np
import pickle

def haversine(lat1, lon1, lat2, lon2):
    # Radio de la Tierra en km
    R = 6371.0
    # Convertir coordenadas de grados a radianes
    lat1, lon1, lat2, lon2 = map(radians, [lat1, lon1, lat2, lon2])
    
    # Diferencia de coordenadas
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    
    # Fórmula de Haversine
    a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))
    
    # Distancia en km

    distancia = R * c
    return distancia

# Datos de cadetes
cadetes_ids = [169, 350, 194, 374, 278, 396, 380, 221, 231, 398, 258]
colors = {
    169: 'red',
    350: 'blue',
    194: 'green',
    374: 'purple',
    278: 'orange',
    396: 'darkred',
    380: 'lightred',
    221: 'beige',
    231: 'darkblue',
    398: 'darkgreen',
    258: 'cadetblue'
}
# Datos de negocios con coordenadas
negocios = pd.DataFrame({
    "id": [25, 31, 44, 48, 50, 60, 62, 87, 105, 112, 121, 148],
    "nombre": ["Helados Mora", "Churros Dubai", "Grido Helados", "Catrina Resto", "La Chiara",
               "YPF Full Comidas", "Pizza Jet", "La Simoqueña", "El Rey De La Milanesa", "Bon Appetit",
               "Planeta Empanada", "La Cerrajería"],
    "latitud": [-38.987044, -38.987747, -38.982327, -38.988396, -38.986982, -38.975398, -38.987308,
                -38.986986, -38.985320, -38.987546, -38.985864, -38.986225],
    "longitud": [-61.303534, -61.278125, -61.287316, -61.289096, -61.303002, -61.289917, -61.285160,
                 -61.304143, -61.289007, -61.284714, -61.285876, -61.286789]
})

# Datos de direcciones de entrega con latitudes y longitudes reales
direcciones = pd.DataFrame({
    "id": range(6003, 6053),
    "direccion": [
        "1 de Abril, null", "Puerto Madryn 316", "Zucchiati 1285", "San Martín 857", "San Lorenzo 81",
        "Córdoba 487 casa 2", "1 de Abril 680 , entre rio Atuel y gregorio Juares", "Lucinda Sutton 360",
        "Antonio costa 865", "Antonio costa 864", "Chaco 72, Monte Hermoso", "Costa 900", "Saavedra lamas 38",
        "Deluster753 casa53", "Majluf 564", "Río salado72", "ACONCAGUA  225", "Las ballenas 484",
        "Traful bis 350", "LUCINDa SUTTOn 944", "Rudocindo paez 576, Monte Hermoso", "Chaco 693",
        "Av.faro recalada 1460", "Luzuriaga 568", "Majluf 587", "Puerto madryn 561", "Puerto madryn y pampa",
        "Puerto madryn 651", "Cordoba 595", "Cordoba 595", "595", "gregorio juarez 127", "Formosa 764",
        "Piedrabuena 600", "Las Dunas 440, Monte Hermoso", "Luzuriaga 119", "LOS ZORZALES 714",
        "Las acacias 358", "Río Asunción 909", "Juan domingo peron 242, Monte Hermoso", "Juan domingo peron 242, Monte Hermoso",
        "Dorrego 374", "Lucinda Sutton 686", "Favaloro 658", "Rene favaloro 658", "San Martin 1228", "Bernardo Houssay 63",
        "Santa cruz 508", "Santa cruz 508", "Dufaur 74"
    ],
    "latitud": [
        -38.978914, -38.979788, -38.984219, -38.981093, -38.985331, -38.982982, -38.978832, -38.983438, -38.980573,
        -38.980615, -38.9882255, -38.979863, -38.977682, -38.979985, -38.983678, -38.987921, -38.983499, -38.984055,
        -38.986619, -38.983099, -38.9790591, -38.982229, -38.986927, -38.983451, -38.983747, -38.979541, -38.979655,
        -38.979580, -38.982465, -38.982465, -38.982467, -38.987410, -38.981518, -38.982887, -38.986637, -38.987740,
        -38.982751, -38.981846, -38.980961, -38.9892436, -38.9892436, -38.985667, -38.983281, -38.982422, -38.982422,
        -38.980935, -38.978641, -38.983543, -38.983630, -38.988611
    ],
    "longitud": [
        -61.296830, -61.291384, -61.322750, -61.298591, -61.286082, -61.321410, -61.296606, -61.292618, -61.293424,
        -61.293557, -61.300743, -61.293337, -61.289752, -61.297383, -61.286967, -61.312699, -61.286664, -61.323026,
        -61.285209, -61.299819, -61.298694, -61.300321, -61.305795, -61.298208, -61.286555, -61.296384, -61.294663,
        -61.296306, -61.321423, -61.321423, -61.321564, -61.295913, -61.299339, -61.306839, -61.283024, -61.298249,
        -61.276838, -61.284538, -61.308326, -61.2903831, -61.2903831, -61.288025, -61.296894, -61.305058, -61.305058,
        -61.303051, -61.289416, -61.309355, -61.309283, -61.286924
    ]
})


# Generar 20 pedidos aleatorios
pedidos = pd.DataFrame({
    "cadete_id": np.random.choice(cadetes_ids, 20),
    "negocio_id": np.random.choice(negocios['id'], 20),
    "direccion_id": np.random.choice(direcciones['id'], 20, replace=False),
    "hora_del_dia": np.random.randint(23, 24, size=20),
    "dia_de_la_semana": np.random.randint(6, 7, size=20),
})

# Añadir coordenadas de negocios y direcciones a los pedidos
pedidos = pedidos.merge(negocios, left_on='negocio_id', right_on='id', suffixes=('', '_neg'))
pedidos = pedidos.merge(direcciones, left_on='direccion_id', right_on='id', suffixes=('', '_dir'))
pedidos.drop(columns=['id', 'id_dir'], inplace=False)

# Calcular la cantidad de pedidos simultaneos por cadete
# # conteo_pedidos = pedidos_activos['cadete_id']
# # conteo_pedidos.columns = ['cadete_id', 'pedidos_simultaneos']
# #  = pedidos.apply
conteo_pedidos = pedidos['cadete_id'].value_counts().reset_index()
conteo_pedidos.columns = ['cadete_id', 'pedidos_simultaneos']
pedidos['pedidos_simultaneos'] = pedidos.merge(conteo_pedidos, left_on='cadete_id', right_on='cadete_id')['pedidos_simultaneos']

coordinates = pedidos.apply(lambda x: [x['latitud'], x['longitud']], axis=1).tolist()
coordinates_entrega = pedidos.apply(lambda x: [x['latitud_dir'], x['longitud_dir']], axis=1).tolist()

# Crear un mapa centrado en la primera ubicación del arreglo
p = folium.Map(location=[-38.9805793,-61.3048407], zoom_start=14)
n = folium.Map(location=[-38.9805793,-61.3048407], zoom_start=14)
c = folium.Map(location=[-38.9805793,-61.3048407], zoom_start=14)


# Agregar marcadores para cada ubicación en el arreglo
# for coord in coordinates:
#     folium.Marker(location=coord, popup=['name'],icon=folium.DivIcon(html=f"""<svg fill="#FF0054" stroke="#FF0054" height="50px" width="50px" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" 
# 	 viewBox="0 0 511 511" xml:space="preserve">
# <g>
# 	<path d="M503.5,440H479V207.433c13.842-3.487,24-16.502,24-31.933v-104c0-8.547-6.953-15.5-15.5-15.5h-464
# 		C14.953,56,8,62.953,8,71.5v104c0,15.432,10.158,28.446,24,31.933V440H7.5c-4.142,0-7.5,3.358-7.5,7.5s3.358,7.5,7.5,7.5h496
# 		c4.142,0,7.5-3.358,7.5-7.5S507.642,440,503.5,440z M488,71.5v104c0,9.383-6.999,17.384-15.602,17.834
# 		c-4.595,0.235-8.939-1.36-12.254-4.505c-3.317-3.148-5.145-7.4-5.145-11.971V71h32.5C487.776,71,488,71.224,488,71.5z M71,71h33
# 		v105.858c0,9.098-7.402,16.5-16.5,16.5s-16.5-7.402-16.5-16.5V71z M119,71h33v105.858c0,9.098-7.402,16.5-16.5,16.5
# 		s-16.5-7.402-16.5-16.5V71z M167,71h33v105.858c0,9.098-7.402,16.5-16.5,16.5s-16.5-7.402-16.5-16.5V71z M215,71h33v105.858
# 		c0,9.098-7.402,16.5-16.5,16.5s-16.5-7.402-16.5-16.5V71z M263,71h33v105.858c0,9.098-7.402,16.5-16.5,16.5s-16.5-7.402-16.5-16.5
# 		V71z M311,71h33v105.858c0,9.098-7.402,16.5-16.5,16.5s-16.5-7.402-16.5-16.5V71z M359,71h33v105.858c0,9.098-7.402,16.5-16.5,16.5
# 		s-16.5-7.402-16.5-16.5V71z M407,71h33v105.858c0,9.098-7.402,16.5-16.5,16.5s-16.5-7.402-16.5-16.5V71z M23,175.5v-104
# 		c0-0.276,0.224-0.5,0.5-0.5H56v105.858c0,4.571-1.827,8.823-5.145,11.971c-3.314,3.146-7.663,4.743-12.254,4.505
# 		C29.999,192.884,23,184.883,23,175.5z M47,207.462c5.266-1.279,10.128-3.907,14.181-7.753c0.822-0.78,1.599-1.603,2.326-2.462
# 		c5.782,6.793,14.393,11.11,23.993,11.11c9.604,0,18.218-4.32,24-11.119c5.782,6.799,14.396,11.119,24,11.119s18.218-4.32,24-11.119
# 		c5.782,6.799,14.396,11.119,24,11.119s18.218-4.32,24-11.119c5.782,6.799,14.396,11.119,24,11.119s18.218-4.32,24-11.119
# 		c5.782,6.799,14.396,11.119,24,11.119s18.218-4.32,24-11.119c5.782,6.799,14.396,11.119,24,11.119s18.218-4.32,24-11.119
# 		c5.782,6.799,14.396,11.119,24,11.119s18.218-4.32,24-11.119c5.782,6.799,14.396,11.119,24,11.119c9.6,0,18.21-4.317,23.993-11.11
# 		c0.728,0.859,1.504,1.682,2.326,2.462c4.054,3.847,8.914,6.482,14.181,7.761V440h-33V263.5c0-8.547-6.953-15.5-15.5-15.5h-96
# 		c-8.547,0-15.5,6.953-15.5,15.5V440H47V207.462z M416,440h-97V263.5c0-0.276,0.224-0.5,0.5-0.5h96c0.276,0,0.5,0.224,0.5,0.5V440z"
# 		/>
# 	<path d="M343.5,336c-4.142,0-7.5,3.358-7.5,7.5v16c0,4.142,3.358,7.5,7.5,7.5s7.5-3.358,7.5-7.5v-16
# 		C351,339.358,347.642,336,343.5,336z"/>
# 	<path d="M262.5,248h-174c-4.687,0-8.5,3.813-8.5,8.5v142c0,4.687,3.813,8.5,8.5,8.5h174c4.687,0,8.5-3.813,8.5-8.5v-142
# 		C271,251.813,267.187,248,262.5,248z M256,392H95V263h161V392z"/>
# </g>
# </svg>""")).add_to(m)

for idx, row in negocios.iterrows():
    folium.Marker(
        location=[row['latitud'], row['longitud']],
        popup=row['nombre'],
        icon=folium.Icon(color='red', icon='info-sign')
    ).add_to(n)

for idx, row in pedidos.iterrows():
    folium.Marker(
        location=[row['latitud_dir'], row['longitud_dir']],
        popup=str(row['cadete_id'])+ '\n simultaneos:'+str(row['pedidos_simultaneos']),
        icon=folium.Icon(color=colors[row['cadete_id']], icon='info-sign')
    ).add_to(p)

cadetes_idle = [cadete for cadete in cadetes_ids if cadete not in pedidos['cadete_id'].unique()]
if cadetes_idle:
        folium.Marker(
            location=[-38.9805793,-61.3048407],
            popup=str(cadetes_idle) + '\n Estan idle',
            icon=folium.Icon(color='yellow', icon='info-sign')
        ).add_to(c)

# for coord in coordinates_entrega:
#     folium.Marker(location=coord, popup=['name']).add_to(m)

# Guardar el mapa en un archivo HTML

n.save('map-negocios.html')
p.save('map-pedidos.html')
c.save('map-cadetes.html')

# Cargar el modelo desde el archivo
with open('random_forest_model.pkl', 'rb') as file:
    loaded_model = pickle.load(file)

# Usar el modelo cargado para hacer una predicción
# Recorrer cada cadete y sus pedidos
for cadete_id, group in pedidos.groupby('cadete_id'):
    for i in range(0, len(group)):
        current_idx = group.index[i]
        previous_idx = group.index[i-1]
        pedidos.at[current_idx, 'last_delivery_lat'] = pedidos.at[current_idx, 'latitud_dir']
        pedidos.at[current_idx, 'last_delivery_lon'] = pedidos.at[previous_idx, 'longitud_dir']

        # Calcular la distancia desde el último lugar de entrega hasta el nuevo negocio
        if pd.notna(pedidos.at[previous_idx, 'latitud_dir']) and pd.notna(pedidos.at[previous_idx, 'longitud_dir']):
            distancia = haversine(
                pedidos.at[current_idx, 'latitud'], 
                pedidos.at[current_idx, 'longitud'],
                pedidos.at[previous_idx, 'latitud_dir'],
                pedidos.at[previous_idx, 'longitud_dir']
            )
            pedidos.at[current_idx, 'distance_to_next_pickup'] = distancia

pedidos['distance_to_next_pickup'].fillna(0, inplace=False)



idle = [0] * len(cadetes_idle)


pedido_nuevo = pd.DataFrame({
    "negocio_id": np.random.choice(negocios['id'], 1),
    "direccion_id": np.random.choice(direcciones['id'], 1),
    "hora_del_dia": np.random.randint(23, 24, size=1),
    "dia_de_la_semana": np.random.randint(6, 7, size=1),
}, columns=['negocio_id', 'hora_del_dia', 'dia_de_la_semana'])

sin_duplicados = pedidos.groupby('cadete_id').tail(1)

simultaneos_cadetes = pd.DataFrame({
   'cadete_id': sin_duplicados['cadete_id'].tolist() +  cadetes_idle,
   'pedidos_en_simultaneo':  sin_duplicados['pedidos_simultaneos'].tolist() + idle,
   'last_delivery_lat': sin_duplicados['last_delivery_lat'].tolist() + [-38.9805793] * len(cadetes_idle),
   'last_delivery_lon': sin_duplicados['last_delivery_lon'].tolist() + [-61.3048407] * len(cadetes_idle),
},)

negocios_index = negocios.set_index('id', inplace=False)  # Establecer 'id' como index si aún no lo es

# Suponiendo que 'pedido_nuevo' tiene las claves correctas
negocio_id = pedido_nuevo['negocio_id'][0]  # Obtén el ID del negocio del nuevo pedido
def calcular_distancia(row):
    result = negocios_index.loc[negocio_id]
    lat_negocio = result['latitud']
    lon_negocio = result['longitud']
    distancia = haversine(lat_negocio, lon_negocio, row['last_delivery_lat'], row['last_delivery_lon'])
    return distancia

# simultaneos_cadetes['last_delivery_lat'].fillna(-38.9805793, inplace=True)
# simultaneos_cadetes['last_delivery_lon'].fillna(-61.296830, inplace=True)

# Aplica la función a cada fila del DataFrame de cadetes simultáneos
simultaneos_cadetes['distance_to_next_pickup'] = simultaneos_cadetes.apply(calcular_distancia, axis=1)
simultaneos_cadetes['hora_del_dia'] = pedido_nuevo['hora_del_dia'].tolist() * len(simultaneos_cadetes)
simultaneos_cadetes['dia_de_la_semana'] = pedido_nuevo['dia_de_la_semana'].tolist() * len(simultaneos_cadetes)

simultaneos_cadetes['distance_to_next_pickup'].fillna(0, inplace=True)

print(pedidos.sort_values(by='cadete_id', ascending=False))


print(simultaneos_cadetes.sort_values(by='pedidos_en_simultaneo', ascending=False))
# Predicción usando el modelo cargado
predicciones = pd.DataFrame({
    'scoring': loaded_model.predict(simultaneos_cadetes[['distance_to_next_pickup', 'hora_del_dia', 'dia_de_la_semana', 'pedidos_en_simultaneo']]),
    'id': simultaneos_cadetes['cadete_id']
})

# predicciones = simultaneos_cadetes.apply(lambda x: loaded_model.predict(
#      pd.DataFrame({
#         'pedidos_en_simultaneo': x['pedidos_en_simultaneo'],
#         'hora_del_dia': pedido_nuevo['hora_del_dia'],
#         'dia_de_la_semana': pedido_nuevo['dia_de_la_semana'],
#         'distancia_nuevo_pedido': haversine(negocios.loc[negocios['id'] == pedido_nuevo['negocio_id']]['latitud'], negocios.loc[negocios['id'] == pedido_nuevo['negocio_id']]['longitud'], pedido_nuevo['last_delivery_lat'], x['last_delivery_lon'])
#      })
# ), axis=1).tolist()    
print(pedido_nuevo)
print(predicciones.sort_values(by='scoring', ascending=False))

datos_entrada = pd.DataFrame({
    'pedidos_en_simultaneo': [0],
    'distance_to_next_pickup': [1.15],
    'hora_del_dia': [23],
    'dia_de_la_semana': [6]
})

predicciones = loaded_model.predict(datos_entrada[['distance_to_next_pickup', 'hora_del_dia', 'dia_de_la_semana', 'pedidos_en_simultaneo']])


