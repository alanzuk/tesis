import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_absolute_error, mean_squared_error, r2_score
from math import radians, cos, sin, sqrt, atan2
from datetime import datetime
from sklearn.preprocessing import StandardScaler


def haversine(lat1, lon1, lat2, lon2):
    # Radio de la Tierra en km
    R = 6371.0
    
    # Convertir coordenadas de grados a radianes
    lat1, lon1, lat2, lon2 = map(radians, [lat1, lon1, lat2, lon2])
    
    # Diferencia de coordenadas
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    
    # Fórmula de Haversine
    a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))
    
    # Distancia en km
    distancia = R * c
    
    return distancia

# def calculate_scoring(row):
#     # Asumiendo que valores más bajos en estas métricas son mejores y queremos invertir su efecto en el scoring
#     score = (row['distancia'] * 10 +  # Ponderación mayor a la distancia
#              (row['tiempo_entrega_minutos'] / 10))
#     if row['pedidos_en_simultaneo'] > 0:
#         score = score - row['pedidos_en_simultaneo'] * 2
#     else:
#         score = score + 30
#     return score  # Escalar el resultado para que sea más manejable

def parse_duration_to_minutes(time_str):
    # Asumiendo que el formato es HH:MM:SS y puede incluir un signo negativo
    sign = -1 if time_str.startswith('-') else 1
    time_str = time_str.strip('-')  # Remover el signo para el parsing

    # Parsear el tiempo a un objeto datetime
    td = datetime.strptime(time_str, "%H:%M:%S")

    # Calcular los minutos totales
    total_minutes = sign * (td.hour * 60 + td.minute + td.second / 60)

    return total_minutes

def calculate_scoring(row, min_score=0, max_score=100):
    score = 100
    # Asumiendo que valores más bajos en estas métricas son mejores y queremos invertir su efecto en el scoring
    score = score - row['distance_to_next_pickup'] * 20
    if row['pedidos_en_simultaneo'] > 0:
        score = score - row['pedidos_en_simultaneo'] * 20

    # Normalización Min-Max para ajustar el score entre 0 y 1
    normalized_score = (score - min_score) / (max_score - min_score)
    normalized_score = max(0, min(1, normalized_score))  # Asegurar que el valor esté entre 0 y 1

    return normalized_score

# Cargar los archivos CSV
pedidos_path = pd.read_csv('./historico-pedidos-cadetes.csv')

# Convertir las columnas relevantes a float64 en ambos DataFrames antes del merge
pedidos_path['negocio_latitud'] = pd.to_numeric(pedidos_path['negocio_latitud'], errors='coerce')
pedidos_path['negocio_longitud'] = pd.to_numeric(pedidos_path['negocio_longitud'], errors='coerce')
pedidos_path['entrega_latitud'] = pd.to_numeric(pedidos_path['entrega_latitud'], errors='coerce')
pedidos_path['entrega_longitud'] = pd.to_numeric(pedidos_path['entrega_longitud'], errors='coerce')

# Asumiendo que distancias_path es un DataFrame y no una ruta de archivo. Si es una ruta de archivo, primero necesitas cargarlo:
# distancias = pd.read_csv(distancias_path)

# Ahora realiza el merge
# pedidos_con_distancias = pd.merge(pedidos_negocios_users, distancias_path, left_on=['latitud_x', 'longitud_x', 'latitud_y', 'longitud_y'],
#                                   right_on=['latitud_origen', 'longitud_origen', 'latitud_destino', 'longitud_destino'], how='left')
pedidos_con_distancias = pedidos_path.copy()
pedidos_con_distancias['distancia'] = pedidos_con_distancias.apply(lambda row: haversine(row['negocio_longitud'], row['negocio_latitud'],row['entrega_longitud'], row['entrega_latitud']), axis=1)
pedidos_con_distancias.to_csv('./pedidos_con_distancias_cadetes.csv', index=False)

# # Convertir las columnas de tiempo a datetime
pedidos_con_distancias['pedido_created_at'] = pd.to_datetime(pedidos_con_distancias['pedido_created_at'])
pedidos_con_distancias['pedido_updated_at'] = pd.to_datetime(pedidos_con_distancias['pedido_updated_at'])

# # Filtrar solo pedidos finalizados y calcular el tiempo de entrega en minutos
combined_data_clean = pedidos_con_distancias.dropna(subset=['distancia']).copy()

# Asegúrate de que 'created_at' esté en formato datetime
# Extraer la hora del día
combined_data_clean.loc[:,'hora_del_dia'] = combined_data_clean['pedido_created_at'].dt.hour

# Extraer el día de la semana
combined_data_clean.loc[:,'dia_de_la_semana'] = combined_data_clean['pedido_created_at'].dt.dayofweek

pedidos_en_simultaneo = []

# Inicializar un diccionario para mantener los pedidos activos por negocio_id.
pedidos_activos_por_cadete = {}

for index, row in combined_data_clean.iterrows():
    cadete_id = row['cadete_id']
    created_at = row['pedido_created_at']
    updated_at = row['pedido_updated_at']

    # # Asegurarse de que el negocio_id esté en el diccionario.
    if cadete_id not in pedidos_activos_por_cadete:
        pedidos_activos_por_cadete[cadete_id] = []

    # Contar pedidos en simultáneo: todos los que empezaron pero no han terminado antes de este.
    pedidos_simultaneos = len([p for p in pedidos_activos_por_cadete[cadete_id] if p < created_at])
    pedidos_en_simultaneo.append(pedidos_simultaneos)

    # Actualizar la lista de pedidos activos para este negocio.
    # Añadir el updated_at del pedido actual al registro de pedidos activos.
    pedidos_activos_por_cadete[cadete_id].append(updated_at)

    # Eliminar pedidos que ya no están activos, es decir, cuyo updated_at es anterior al created_at actual.
    pedidos_activos_por_cadete[cadete_id] = [p for p in pedidos_activos_por_cadete[cadete_id] if p > created_at]

# Asignar la lista de pedidos en simultáneo al DataFrame como una nueva columna.
combined_data_clean['pedidos_en_simultaneo'] = pedidos_en_simultaneo

combined_data_clean.sort_values(by=['cadete_id', 'pedido_created_at'], inplace=True)

# Inicializar las columnas necesarias
combined_data_clean['last_delivery_lat'] = np.nan
combined_data_clean['last_delivery_lon'] = np.nan
combined_data_clean['distance_to_next_pickup'] = np.nan

# Recorrer cada cadete y sus pedidos
for cadete_id, group in combined_data_clean.groupby('cadete_id'):
    for i in range(1, len(group)):
        current_idx = group.index[i]
        previous_idx = group.index[i-1]

        # Asignar latitud y longitud del último lugar de entrega al actual
        combined_data_clean.at[current_idx, 'last_delivery_lat'] = combined_data_clean.at[previous_idx, 'entrega_latitud']
        combined_data_clean.at[current_idx, 'last_delivery_lon'] = combined_data_clean.at[previous_idx, 'entrega_longitud']

        # Calcular la distancia desde el último lugar de entrega hasta el nuevo negocio
        if pd.notna(combined_data_clean.at[previous_idx, 'entrega_latitud']) and pd.notna(combined_data_clean.at[previous_idx, 'entrega_longitud']):
            distancia = haversine(
                combined_data_clean.at[current_idx, 'negocio_latitud'], 
                combined_data_clean.at[current_idx, 'negocio_longitud'],
                combined_data_clean.at[previous_idx, 'entrega_latitud'],
                combined_data_clean.at[previous_idx, 'entrega_longitud']
            )
            combined_data_clean.at[current_idx, 'distance_to_next_pickup'] = distancia


combined_data_clean['scoring'] = combined_data_clean.apply(lambda row: calculate_scoring(row), axis=1)
# combined_data_clean['tiempo_envio'] = combined_data_clean.apply(lambda row: parse_duration_to_minutes(str(row['tiempo_envio'])), axis=1)

# combined_data_clean['delivery_time_diff'] = combined_data_clean.groupby('pedido_id').apply(calculate_time).reset_index(level=0, drop=True)
# scaler = StandardScaler()
# features = ['distancia', 'pedidos_en_simultaneo', 'hora_del_dia', 'dia_de_la_semana']
# combined_data_clean[features] = scaler.fit_transform(combined_data_clean[features])

# Limpieza básica y selección de características
features = ['pedidos_en_simultaneo', 'hora_del_dia', 'dia_de_la_semana', 'distance_to_next_pickup', 'tiempo_envio']

if combined_data_clean[features].isnull().values.any():
    print("NaNs found in features.")
if np.isinf(combined_data_clean[features].values).any():
    print("Infs found in features.")

# # Reemplazar NaNs e Infinitos
combined_data_clean.replace([np.inf, -np.inf], np.nan, inplace=True)
combined_data_clean.dropna(inplace=True)  # O imputa según convenga

combined_data_clean.to_csv('./combined_data_clean_cadetes.csv', index=False)

# X = combined_data_clean[['distancia']] # Asegúrate de que 'distancia' esté en tus datos



# # Dividir los datos en entrenamiento y prueba
# X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# # Entrenar el modelo
# modelo = RandomForestRegressor(n_estimators=100, random_state=42)
# modelo.fit(X_train, y_train)

# # Realizar predicciones y evaluar
# predicciones = modelo.predict(X_test)
# mae = mean_absolute_error(y_test, predicciones)
# mse = mean_squared_error(y_test, predicciones)
# rmse = np.sqrt(mse)
# r2 = r2_score(y_test, predicciones)

# print(f'MAE: {mae}, MSE: {mse}, RMSE: {rmse}, R²: {r2}')

# Ajusta las rutas a los archivos CSV y asegúrate de que las columnas coincidan con tus datos.
