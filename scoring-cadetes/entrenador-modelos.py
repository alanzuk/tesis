import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestRegressor, GradientBoostingRegressor
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_absolute_error, mean_squared_error, r2_score
import xgboost as xgb

# Asumimos que los datos ya están limpios y preparados en combined_data_clean.csv
combined_data_clean = pd.read_csv('./combined_data_clean_cadetes.csv')

# Convertir las columnas de tiempo a datetime (si es necesario)
# combined_data_clean['created_at'] = pd.to_datetime(combined_data_clean['created_at'])
# combined_data_clean['updated_at'] = pd.to_datetime(combined_data_clean['updated_at'])

# Preparación de los datos para el modelo
# X = combined_data_clean[['distancia']]
X = combined_data_clean[['distance_to_next_pickup', 'hora_del_dia', 'dia_de_la_semana', 'pedidos_en_simultaneo', 'tiempo_envio']]
y = combined_data_clean['scoring']

# Dividir los datos en entrenamiento y prueba
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Modelos a evaluar
modelos = {
    "Random Forest": RandomForestRegressor(n_estimators=100, random_state=42),
    "Gradient Boosting": GradientBoostingRegressor(n_estimators=100, random_state=42),
    "XGBoost": xgb.XGBRegressor(objective ='reg:squarederror', n_estimators=100, seed=42),
    "Linear Regression": LinearRegression()
}

# Entrenar, predecir y evaluar cada modelo
for nombre, modelo in modelos.items():
    modelo.fit(X_train, y_train)
    predicciones = modelo.predict(X_test)
    mae = mean_absolute_error(y_test, predicciones)
    mse = mean_squared_error(y_test, predicciones)
    rmse = np.sqrt(mse)
    r2 = r2_score(y_test, predicciones)
    print(f"{nombre} - MAE: {mae}, MSE: {mse}, RMSE: {rmse}, R²: {r2}")


# Asumiendo que tienes las siguientes variables para el nuevo pedido:
distancia_nuevo_pedido = 0.81  # Ejemplo de distancia calculada
hora_del_dia_nuevo_pedido = 23  # 2 PM
dia_de_la_semana_nuevo_pedido = 3  # Miércoles
pedidos_en_simultaneo = 2
tiempo_envio = 40

# # Crear un DataFrame o arreglo con estas características
nuevas_caracteristicas = pd.DataFrame([[distancia_nuevo_pedido, hora_del_dia_nuevo_pedido, dia_de_la_semana_nuevo_pedido, pedidos_en_simultaneo, tiempo_envio]],
                                       columns=['distance_to_next_pickup', 'hora_del_dia', 'dia_de_la_semana', 'pedidos_en_simultaneo', 'tiempo_envio'])

# Si aplicaste algún tipo de normalización o transformación, asegúrate de aplicarla también aquí
for nombre, modelo in modelos.items():
    # Asegúrate de pasar las características en el formato correcto que espera el modelo
    # Para la red neuronal, podría ser necesario agregar una dimensión extra si espera un batch
    # Para otros modelos, asegúrate de que las características estén escaladas si fue necesario durante el entrenamiento
    predicciones_nuevo_pedido = modelo.predict(nuevas_caracteristicas)
    
    # Imprime la predicción para cada modelo
    print(f"Predicción de {nombre}: {predicciones_nuevo_pedido[0]} minutos")
