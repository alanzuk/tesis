import pandas as pd
from sklearn.preprocessing import StandardScaler
import numpy as np
from keras.models import load_model

# Definir los datos de entrada
datos_entrada = {
    'distancia': [21.1525821899036628],
    'tiempo_entrega_minutos': [152.166666666666664],
    'pedidos_en_simultaneo': [12],
    'hora_del_dia': [18],
    'dia_de_la_semana': [6]
}

# Crear DataFrame
df_entrada = pd.DataFrame(datos_entrada)

# Asumiendo que ya has ajustado el StandardScaler con tus datos de entrenamiento y lo has guardado
# Cargar el StandardScaler guardado o inicializar uno nuevo y ajustar solo para demostración (en la práctica debes usar el ajustado durante el entrenamiento)
scaler = StandardScaler()
df_entrada_scaled = scaler.fit_transform(df_entrada)

### Paso 2: Cargar el Modelo
# Cargar el modelo entrenado
model = load_model('modelo_entrenado.keras')

### Paso 3: Hacer Predicciones
# Hacer la predicción usando el modelo cargado
prediction = model.predict(df_entrada_scaled)
print(f"La puntuación predicha es: {prediction[0][0]}")
