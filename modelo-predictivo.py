import pandas as pd
from sklearn.preprocessing import StandardScaler
import numpy as np
from keras.models import load_model

# Definir los datos de entrada
datos_entrada = {
    'pedidos_en_simultaneo': [5],
    'distance_to_next_pickup': [1.15],
    'tiempo_envio': [40],
     'hora_del_dia': [21],
     'dia_de_la_semana': [5]

}

# Crear DataFrame
df_entrada = pd.DataFrame(datos_entrada)

# Asumiendo que ya has ajustado el StandardScaler con tus datos de entrenamiento y lo has guardado
# Cargar el StandardScaler guardado o inicializar uno nuevo y ajustar solo para demostración (en la práctica debes usar el ajustado durante el entrenamiento)
scaler = StandardScaler()
df_entrada_scaled = scaler.fit_transform(df_entrada)

### Paso 2: Cargar el Modelo
# Cargar el modelo entrenado
model = load_model('modelo_entrenado.keras')

### Paso 3: Hacer Predicciones
# Hacer la predicción usando el modelo cargado
prediction = model.predict(df_entrada_scaled)
print(f"La puntuación predicha es: {prediction[0][0]}")
