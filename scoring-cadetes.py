import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from keras.models import Sequential
from keras.layers import Dense

# Cargar datos
data = pd.read_csv('./combined_data_clean_cadetes.csv')

# Normalización de las características numéricas
scaler = StandardScaler()
features = ['pedidos_en_simultaneo', 'distance_to_next_pickup', 'tiempo_envio', 'hora_del_dia', 'dia_de_la_semana']
data[features] = scaler.fit_transform(data[features])

# Dividir los datos en conjuntos de entrenamiento, validación y prueba
X_train, X_temp, y_train, y_temp = train_test_split(data[features], data['scoring'], test_size=0.3, random_state=42)
X_val, X_test, y_val, y_test = train_test_split(X_temp, y_temp, test_size=0.5, random_state=42)

# Construir el modelo de red neuronal
model = Sequential()
model.add(Dense(64, input_dim=len(features), activation='relu'))  # Capa de entrada y primera capa oculta
model.add(Dense(32, activation='relu'))  # Segunda capa oculta
model.add(Dense(1, activation='linear'))  # Capa de salida

# Compilar el modelo
model.compile(loss='mean_squared_error',  # Cambiar por 'binary_crossentropy' para clasificación
              optimizer='adam',
              metrics=['mean_squared_error'])

# Entrenar el modelo
history = model.fit(X_train, y_train, validation_data=(X_val, y_val), epochs=50, batch_size=10)

# Evaluar el modelo
scores = model.evaluate(X_test, y_test)
print(f"Test Mean Squared Error: {scores[1]}")

# Opcional: Guardar el modelo entrenado
model.save('modelo_entrenado.keras')
