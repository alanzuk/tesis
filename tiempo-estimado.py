import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_absolute_error, mean_squared_error, r2_score
from math import radians, cos, sin, sqrt, atan2

def haversine(lat1, lon1, lat2, lon2):
    # Radio de la Tierra en km
    R = 6371.0
    
    # Convertir coordenadas de grados a radianes
    lat1, lon1, lat2, lon2 = map(radians, [lat1, lon1, lat2, lon2])
    
    # Diferencia de coordenadas
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    
    # Fórmula de Haversine
    a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))
    
    # Distancia en km
    distancia = R * c
    
    return distancia


# Cargar los archivos CSV
pedidos_path = pd.read_csv('./historico-total-grido.csv')
distancias_path = pd.read_csv('./distancias-2022.csv')
# Asumiendo que tienes un archivo para negocios
negocios = pd.read_csv('./negocios-monte.csv')
direcciones = pd.read_csv('./direcciones_users.csv')

# Combinar los datos de pedidos con los negocios para obtener latitud y longitud de origen
pedidos_con_negocios = pd.merge(pedidos_path, negocios, left_on='negocio_id', right_on='id', how='left')

pedidos_negocios_users = pd.merge(pedidos_con_negocios, direcciones, left_on='user_id', right_on='user_id', how='left')

# Combinar los datos de pedidos con las distancias usando las coordenadas de origen y destino
# Aquí asumimos que distancias.csv tiene columnas 'lat_origen', 'lng_origen', 'lat_destino', 'lng_destino'
# Necesitarás ajustar los nombres de columnas según tus archivos
pedidos_negocios_users.to_csv('./pedidos_negocios_users.csv', index=False)

# Convertir las columnas relevantes a float64 en ambos DataFrames antes del merge
pedidos_negocios_users['latitud_x'] = pd.to_numeric(pedidos_negocios_users['latitud_x'], errors='coerce')
pedidos_negocios_users['longitud_x'] = pd.to_numeric(pedidos_negocios_users['longitud_x'], errors='coerce')
pedidos_negocios_users['latitud_y'] = pd.to_numeric(pedidos_negocios_users['latitud_y'], errors='coerce')
pedidos_negocios_users['longitud_y'] = pd.to_numeric(pedidos_negocios_users['longitud_y'], errors='coerce')

# Asumiendo que distancias_path es un DataFrame y no una ruta de archivo. Si es una ruta de archivo, primero necesitas cargarlo:
# distancias = pd.read_csv(distancias_path)

# Ahora realiza el merge
# pedidos_con_distancias = pd.merge(pedidos_negocios_users, distancias_path, left_on=['latitud_x', 'longitud_x', 'latitud_y', 'longitud_y'],
#                                   right_on=['latitud_origen', 'longitud_origen', 'latitud_destino', 'longitud_destino'], how='left')
pedidos_con_distancias = pedidos_negocios_users.copy()
pedidos_con_distancias['distancia'] = pedidos_con_distancias.apply(lambda row: haversine(row['longitud_x'], row['latitud_x'],row['longitud_y'], row['latitud_y']), axis=1)

pedidos_con_distancias.to_csv('./pedidos_con_distancias.csv', index=False)
# # Convertir las columnas de tiempo a datetime
pedidos_con_distancias['created_at'] = pd.to_datetime(pedidos_con_distancias['created_at'])
pedidos_con_distancias['updated_at'] = pd.to_datetime(pedidos_con_distancias['tiempo_final'])

# # Filtrar solo pedidos finalizados y calcular el tiempo de entrega en minutos
combined_data = pedidos_con_distancias[pedidos_con_distancias['estado'] == 'Finalizado']
combined_data_clean = combined_data.dropna(subset=['distancia']).copy()


combined_data_clean.loc[:, 'tiempo_entrega_minutos'] = (combined_data_clean['updated_at'] - combined_data_clean['created_at']).dt.total_seconds() / 60
# Asegúrate de que 'created_at' esté en formato datetime
# Extraer la hora del día
combined_data_clean.loc[:,'hora_del_dia'] = combined_data_clean['created_at'].dt.hour

# Extraer el día de la semana
combined_data_clean.loc[:,'dia_de_la_semana'] = combined_data_clean['created_at'].dt.dayofweek

pedidos_en_simultaneo = []

# Inicializar un diccionario para mantener los pedidos activos por negocio_id.
pedidos_activos_por_negocio = {}

for index, row in combined_data_clean.iterrows():
    negocio_id = row['negocio_id']
    created_at = row['created_at']
    updated_at = row['updated_at']

    # # Asegurarse de que el negocio_id esté en el diccionario.
    if negocio_id not in pedidos_activos_por_negocio:
        pedidos_activos_por_negocio[negocio_id] = []

    # Contar pedidos en simultáneo: todos los que empezaron pero no han terminado antes de este.
    pedidos_simultaneos = len([p for p in pedidos_activos_por_negocio[negocio_id] if p < created_at])
    pedidos_en_simultaneo.append(pedidos_simultaneos)

    # Actualizar la lista de pedidos activos para este negocio.
    # Añadir el updated_at del pedido actual al registro de pedidos activos.
    pedidos_activos_por_negocio[negocio_id].append(updated_at)

    # Eliminar pedidos que ya no están activos, es decir, cuyo updated_at es anterior al created_at actual.
    pedidos_activos_por_negocio[negocio_id] = [p for p in pedidos_activos_por_negocio[negocio_id] if p > created_at]

# Asignar la lista de pedidos en simultáneo al DataFrame como una nueva columna.
combined_data_clean['pedidos_en_simultaneo'] = pedidos_en_simultaneo

# Limpieza básica y selección de características
combined_data_clean = combined_data_clean[combined_data_clean['tiempo_entrega_minutos'] <= 150] # Filtrar pedidos mayores a 2.5 horas

combined_data_clean.to_csv('./combined_data_clean.csv', index=False)

X = combined_data_clean[['distancia']] # Asegúrate de que 'distancia' esté en tus datos
y = combined_data_clean['tiempo_entrega_minutos']



# Dividir los datos en entrenamiento y prueba
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Entrenar el modelo
modelo = RandomForestRegressor(n_estimators=100, random_state=42)
modelo.fit(X_train, y_train)

# Realizar predicciones y evaluar
predicciones = modelo.predict(X_test)
mae = mean_absolute_error(y_test, predicciones)
mse = mean_squared_error(y_test, predicciones)
rmse = np.sqrt(mse)
r2 = r2_score(y_test, predicciones)

print(f'MAE: {mae}, MSE: {mse}, RMSE: {rmse}, R²: {r2}')

# Ajusta las rutas a los archivos CSV y asegúrate de que las columnas coincidan con tus datos.
