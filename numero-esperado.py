import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_squared_error, r2_score

# Cargar datos
data_pedidos = pd.read_csv('historico-pedidos-cadetes.csv')
data_cadetes = pd.read_csv('cadetes.csv')
data_cancelados = pd.read_csv('cancelados.csv')

# Convertir columnas de tiempo a datetime si aún no lo están
data_pedidos['hora_pedido'] = pd.to_datetime(data_pedidos['pedido_created_at']).dt.hour
data_pedidos['dia_semana'] = pd.to_datetime(data_pedidos['pedido_created_at']).dt.dayofweek

# Agregar columna de pedidos cancelados
data_pedidos['cancelados'] = 0
data_cancelados['cancelados'] = 1

# Combinar datos de pedidos con los pedidos cancelados
data_total = pd.concat([data_pedidos, data_cancelados], ignore_index=True, sort=False).fillna(0)

# Agregar información de cadetes activos por hora y día
data_cadetes['hora_activa'] = pd.to_datetime(data_cadetes['hora_inicio']).dt.hour
data_cadetes['dia_semana'] = pd.to_datetime(data_cadetes['fecha_inicio']).dt.dayofweek

# Contar número de cadetes por hora y día
cadetes_por_hora = data_cadetes.groupby(['hora_activa', 'dia_semana']).size().reset_index(name='num_cadetes')

# Agregar esta información al DataFrame principal
data_total = pd.merge(data_total, cadetes_por_hora, how='left', left_on=['hora_pedido', 'dia_semana'], right_on=['hora_activa', 'dia_semana'])

# Preparar datos para el modelo
X = data_total[['num_cadetes', 'dia_semana', 'hora_pedido', 'cancelados']]  # Características
y = data_total['num_pedidos']  # Etiqueta

# Dividir los datos en entrenamiento y prueba
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Modelo de Random Forest
model = RandomForestRegressor(n_estimators=100, random_state=42)
model.fit(X_train, y_train)

# Predicciones y evaluación
predictions = model.predict(X_test)
mse = mean_squared_error(y_test, predictions)
r2 = r2_score(y_test, predictions)

print(f"MSE: {mse}")
print(f"R^2: {r2}")

# Predicción para nuevos datos
nuevo_dato = pd.DataFrame({
    'num_cadetes': [10],  # supongamos que queremos evaluar 10 cadetes en un momento dado
    'dia_semana': [3],   # Miércoles
    'hora_pedido': [15], # 3 PM
    'cancelados': [2]    # supongamos que hay 2 pedidos cancelados
})

nueva_prediccion = model.predict(nuevo_dato)
print(f"Número esperado de pedidos: {nueva_prediccion[0]}")
